package Data_Layer.Utenti;

import Data_Layer.MYSQLDAO;

public interface UtentiDAO extends MYSQLDAO{
	
	public Utente getUtente(String username);
 
}