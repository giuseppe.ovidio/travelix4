package Data_Layer.Prenotazioni;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PrenotazioniDAOImpl implements PrenotazioniDAO{

	private static final String CREATE_QUERY = "INSERT INTO prenotazioni (statoPagamento," + 
			" prezzoVoli," + 
			" prezzoHotel," + 
			" prezzoTotale," + 
			" orarioVoloAndataOrigine," + 
			" orarioVoloAndataDestinazione," + 
			" orarioVoloRitornoOrigine," + 
			" orarioVoloRitornoDestinazione," + 
			" dataVoloAndataOrigine," + 
			" dataVoloAndataDestinazione," + 
			" dataVoloRitornoOrigine," + 
			" dataVoloRitornoDestinazione," + 
			" numeroAdulti," + 
			" numeroBambini," + 
			" nomeHotel," + 
			" descrizioneHotel," + 
			" ratingHotel," + 
			" stelleHotel," + 
			" indirizzoHotel," + 
			" immagineHotel," + 
			" luogoOrigine," + 
			" luogoDestinazione," + 
			" numeroHotel," + 
			" username,"+
			" idHotel) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String DELETE_QUERY = "DELETE FROM prenotazioni WHERE idPrenotazione = ?";
	private static final String UPDATE_QUERY = "UPDATE prenotazioni SET statoPagamento=true WHERE idPrenotazione = ?";
	private static final String READ_ALL_QUERY = "SELECT idPrenotazione, statoPagamento, prezzoTotale, "
			+ "dataVoloAndataDestinazione, dataVoloRitornoOrigine, nomeHotel,"
			+ " luogoDestinazione, numeroAdulti, numeroBambini FROM prenotazioni WHERE username = ?";
	private static final String READ_QUERY = "SELECT * FROM prenotazioni WHERE idPrenotazione = ?";

	public Prenotazione getPrenotazione(int idPrenotazione) {

		Prenotazione prenotazione= null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = PrenotazioniDAOFactory.createConnection();
			preparedStatement = conn.prepareStatement(READ_QUERY);
			preparedStatement.setInt(1, idPrenotazione);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				prenotazione = new Prenotazione(
						result.getInt(1),result.getBoolean(2),result.getInt(3),
						result.getInt(4),result.getInt(5),result.getString(6),
						result.getString(7),result.getString(8),result.getString(9),
						result.getString(10),result.getString(11),
						result.getString(12),result.getString(13),result.getInt(14),result.getInt(15),
						result.getString(16),result.getString(17),result.getString(18),
						result.getInt(19),result.getString(20),result.getString(21),
						result.getString(22),result.getString(23),result.getString(24), result.getString(25), result.getInt(26));
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				result.close();
			} catch (Exception rse) {
				rse.printStackTrace();
			}
			try {
				preparedStatement.close();
			} catch (Exception sse) {
				sse.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception cse) {
				cse.printStackTrace();
			}
		}

		return prenotazione;
	}

	public List<Prenotazione> getListaPrenotazioni(String username) {

		List<Prenotazione> prenotazioni = new ArrayList();
		Prenotazione prenotazione = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = PrenotazioniDAOFactory.createConnection();            
			preparedStatement = conn.prepareStatement(READ_ALL_QUERY);
			preparedStatement.setString(1, username);
			preparedStatement.execute();
			result = (ResultSet) preparedStatement.getResultSet();

			while ( result.next()) {            	
				prenotazione =  new Prenotazione(
						result.getInt(1),result.getBoolean(2),result.getInt(3),
						result.getString(4),result.getString(5),result.getString(6),
						result.getString(7),result.getInt(8),result.getInt(9));
				
				prenotazioni.add(prenotazione);
			}     
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				result.close();
			} catch (Exception rse) {
				rse.printStackTrace();
			}
			try {
				preparedStatement.close();
			} catch (Exception sse) {
				sse.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception cse) {
				cse.printStackTrace();
			}
		}

		return prenotazioni;
	}

	public boolean pagaPrenotazione(int idPrenotazione) {

		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = PrenotazioniDAOFactory.createConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, idPrenotazione);
			preparedStatement.execute();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
			} catch (Exception sse) {
				sse.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception cse) {
				cse.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean deletePrenotazione(int idPrenotazione) {
		Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = PrenotazioniDAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1, idPrenotazione);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return false;
	}

	public boolean createPrenotazione(Prenotazione prenotazione) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = PrenotazioniDAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
            
            preparedStatement.setBoolean(1, prenotazione.getStatoPagamento());
            preparedStatement.setInt(2, prenotazione.getPrezzoVoli());
            preparedStatement.setInt(3, prenotazione.getPrezzoHotel());
            preparedStatement.setInt(4, prenotazione.getPrezzoTotale());
            preparedStatement.setString(5, prenotazione.getOrarioVoloAndataOrigine());
            preparedStatement.setString(6, prenotazione.getOrarioVoloAndataDestinazione());
            preparedStatement.setString(7, prenotazione.getOrarioVoloRitornoOrigine());
            preparedStatement.setString(8, prenotazione.getOrarioVoloRitornoDestinazione());
            preparedStatement.setString(9, prenotazione.getDataVoloAndataOrigine());
            preparedStatement.setString(10, prenotazione.getDataVoloAndataDestinazione());
            preparedStatement.setString(11, prenotazione.getDataVoloRitornoOrigine());
            preparedStatement.setString(12, prenotazione.getDataVoloRitornoDestinazione());
            preparedStatement.setInt(13, prenotazione.getNumeroAdulti());
            preparedStatement.setInt(14, prenotazione.getNumeroBambini());
            preparedStatement.setString(15, prenotazione.getNomeHotel());
            preparedStatement.setString(16, prenotazione.getDescrizioneHotel());
            preparedStatement.setString(17, prenotazione.getRatingHotel());
            preparedStatement.setInt(18, prenotazione.getStelleHotel());
            preparedStatement.setString(19, prenotazione.getIndirizzoHotel());
            preparedStatement.setString(20, prenotazione.getImmagineHotel());
            preparedStatement.setString(21, prenotazione.getLuogoOrigine());
            preparedStatement.setString(22, prenotazione.getLuogoDestinazione());
            preparedStatement.setString(23, prenotazione.getNumeroHotel());
            preparedStatement.setString(24, prenotazione.getUsername());
            preparedStatement.setInt(25, prenotazione.getIdHotel());
            preparedStatement.execute();
            result = preparedStatement.getGeneratedKeys();
 
            if (result.next() && result != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return false;
    }
	
}
