package Data_Layer.Prenotazioni;

public class Prenotazione {

	private int idPrenotazione;
	private boolean statoPagamento;
	private int prezzoVoli;
	private int prezzoHotel;
	private int prezzoTotale;
	private String orarioVoloAndataOrigine;
	private String orarioVoloAndataDestinazione;
	private String orarioVoloRitornoOrigine;
	private String orarioVoloRitornoDestinazione;
	private String dataVoloAndataOrigine;
	private String dataVoloAndataDestinazione;
	private String dataVoloRitornoOrigine;
	private String dataVoloRitornoDestinazione;	
	private int numeroAdulti;
	private int numeroBambini;
	private String nomeHotel;
	private String descrizioneHotel;
	private String ratingHotel;
	private int stelleHotel ;
	private String indirizzoHotel;
	private String immagineHotel;
	private String luogoOrigine;
	private String luogoDestinazione;
	private String numeroHotel;
	private String username;
	private int idHotel;

	//Costruttore usato per la get della prenotazione (READ_QUERY)

	public Prenotazione(int idPrenotazione, boolean statoPagamento, int prezzoVoli, int prezzoHotel, int prezzoTotale, 
			String orarioVoloAndataOrigine, String orarioVoloAndataDestinazione, String orarioVoloRitornoOrigine,
			String orarioVoloRitornoDestinazione,
			String dataVoloAndataOrigine, String dataVoloAndataDestinazione, String dataVoloRitornoOrigine,
			String dataVoloRitornoDestinazione,
			int numeroAdulti, int numeroBambini, String nomeHotel, String descrizioneHotel,
			String ratingHotel, int stelleHotel, String indirizzoHotel,  String immagineHotel, String luogoOrigine,
			String luogoDestinazione, String numeroHotel,String username, int idHotel) {
		super();


		this.idPrenotazione=idPrenotazione;
		this.statoPagamento=statoPagamento;
		this.prezzoVoli=prezzoVoli;
		this.prezzoHotel=prezzoHotel;
		this.prezzoTotale=prezzoTotale;
		this.orarioVoloAndataOrigine=orarioVoloAndataOrigine;
		this.orarioVoloAndataDestinazione=orarioVoloAndataDestinazione;
		this.orarioVoloRitornoOrigine=orarioVoloRitornoOrigine;
		this.orarioVoloRitornoDestinazione=orarioVoloRitornoDestinazione;
		this.dataVoloAndataOrigine=dataVoloAndataOrigine;
		this.dataVoloAndataDestinazione=dataVoloAndataDestinazione;
		this.dataVoloRitornoOrigine=dataVoloRitornoOrigine;
		this.dataVoloRitornoDestinazione=dataVoloRitornoDestinazione;
		this.numeroAdulti=numeroAdulti;
		this.numeroBambini=numeroBambini;
		this.nomeHotel=nomeHotel;
		this.descrizioneHotel=descrizioneHotel;
		this.ratingHotel=ratingHotel;
		this.stelleHotel=stelleHotel;
		this.indirizzoHotel=indirizzoHotel;
		this.immagineHotel=immagineHotel;
		this.luogoOrigine=luogoOrigine;
		this.luogoDestinazione=luogoDestinazione;
		this.numeroHotel=numeroHotel;
		this.username=username;
		this.idHotel=idHotel;
	}
	
	/* Costruttore usato per la creazione della prenotazione (usato da PrenotazioneIMPL, lato server, CREATE_QUERY).
	 * L'idPrenotazione non serve, poich� al momento in cui viene effettuata la INSERT nel DB,
	 * esso viene inserito automaticamente (autoincrement)
	 */

	public Prenotazione(int prezzoVoli, int prezzoHotel, int prezzoTotale,
			String orarioVoloAndataOrigine, String orarioVoloAndataDestinazione, String orarioVoloRitornoOrigine,
			String orarioVoloRitornoDestinazione,
			String dataVoloAndataOrigine, String dataVoloAndataDestinazione, String dataVoloRitornoOrigine,
			String dataVoloRitornoDestinazione,
			int numeroAdulti, int numeroBambini, String nomeHotel, String descrizioneHotel,
			String ratingHotel, int stelleHotel, String indirizzoHotel,  String immagineHotel, String luogoOrigine,
			String luogoDestinazione, String numeroHotel, String username, int idHotel) {
		super();

		this.statoPagamento=false; //al momento della creazione, lo stato viene settato automaticamente a false
		this.prezzoVoli=prezzoVoli;
		this.prezzoHotel=prezzoHotel;
		this.prezzoTotale=prezzoTotale;
		this.orarioVoloAndataOrigine=orarioVoloAndataOrigine;
		this.orarioVoloAndataDestinazione=orarioVoloAndataDestinazione;
		this.orarioVoloRitornoOrigine=orarioVoloRitornoOrigine;
		this.orarioVoloRitornoDestinazione=orarioVoloRitornoDestinazione;
		this.dataVoloAndataOrigine=dataVoloAndataOrigine;
		this.dataVoloAndataDestinazione=dataVoloAndataDestinazione;
		this.dataVoloRitornoOrigine=dataVoloRitornoOrigine;
		this.dataVoloRitornoDestinazione=dataVoloRitornoDestinazione;
		this.numeroAdulti=numeroAdulti;
		this.numeroBambini=numeroBambini;
		this.nomeHotel=nomeHotel;
		this.descrizioneHotel=descrizioneHotel;
		this.ratingHotel=ratingHotel;
		this.stelleHotel=stelleHotel;
		this.indirizzoHotel=indirizzoHotel;
		this.immagineHotel=immagineHotel;
		this.luogoOrigine=luogoOrigine;
		this.luogoDestinazione=luogoDestinazione;
		this.numeroHotel=numeroHotel;
		this.username=username;
		this.idHotel=idHotel;
	}
	
	//Costruttore usato per mostrare le prenotazioni compatte (READ_ALL_QUERY)
	
	public Prenotazione(int idPrenotazione, boolean statoPagamento, int prezzoTotale,
			String dataVoloAndataDestinazione, String dataVoloRitornoOrigine,
			String nomeHotel, String luogoDestinazione, int numeroAdulti, int numeroBambini) {
		super();

		this.idPrenotazione=idPrenotazione;
		this.statoPagamento=statoPagamento;
		this.prezzoTotale=prezzoTotale;
		this.dataVoloAndataDestinazione=dataVoloAndataDestinazione;
		this.dataVoloRitornoOrigine=dataVoloRitornoOrigine;
		this.nomeHotel=nomeHotel;
		this.numeroAdulti=numeroAdulti;
		this.numeroBambini=numeroBambini;
		this.luogoDestinazione=luogoDestinazione;
		
	}
	
	public Prenotazione() {
		
	}
	
	
	public int getIdPrenotazione() {
		return idPrenotazione;
	}
	
	
	
	public String getUsername() {
		return username;
	}

	public boolean getStatoPagamento() {
		return statoPagamento;
	}

	public void setStatoPagamento(boolean statoPagamento) {
		this.statoPagamento = statoPagamento;
	}

	public int getPrezzoVoli() {
		return prezzoVoli;
	}

	public void setPrezzoVoli(int prezzoVoli) {
		this.prezzoVoli = prezzoVoli;
	}

	public int getPrezzoHotel() {
		return prezzoHotel;
	}

	public void setPrezzoHotel(int prezzoHotel) {
		this.prezzoHotel = prezzoHotel;
	}

	public int getPrezzoTotale() {
		return prezzoTotale;
	}

	public void setPrezzoTotale(int prezzoTotale) {
		this.prezzoTotale = prezzoTotale;
	}

	public String getOrarioVoloAndataOrigine() {
		return orarioVoloAndataOrigine;
	}

	public void setOrarioVoloAndataOrigine(String orarioVoloAndataOrigine) {
		this.orarioVoloAndataOrigine = orarioVoloAndataOrigine;
	}

	public String getOrarioVoloAndataDestinazione() {
		return orarioVoloAndataDestinazione;
	}

	public void setOrarioVoloAndataDestinazione(String orarioVoloAndataDestinazione) {
		this.orarioVoloAndataDestinazione = orarioVoloAndataDestinazione;
	}

	public String getOrarioVoloRitornoOrigine() {
		return orarioVoloRitornoOrigine;
	}

	public void setOrarioVoloRitornoOrigine(String orarioVoloRitornoOrigine) {
		this.orarioVoloRitornoOrigine = orarioVoloRitornoOrigine;
	}

	public String getOrarioVoloRitornoDestinazione() {
		return orarioVoloRitornoDestinazione;
	}

	public void setOrarioVoloRitornoDestinazione(String orarioVoloRitornoDestinazione) {
		this.orarioVoloRitornoDestinazione = orarioVoloRitornoDestinazione;
	}

	public String getDataVoloAndataOrigine() {
		return dataVoloAndataOrigine;
	}

	public void setDataVoloAndataOrigine(String dataVoloAndataOrigine) {
		this.dataVoloAndataOrigine = dataVoloAndataOrigine;
	}

	public String getDataVoloAndataDestinazione() {
		return dataVoloAndataDestinazione;
	}

	public void setDataVoloAndataDestinazione(String dataVoloAndataDestinazione) {
		this.dataVoloAndataDestinazione = dataVoloAndataDestinazione;
	}

	public String getDataVoloRitornoOrigine() {
		return dataVoloRitornoOrigine;
	}

	public void setDataVoloRitornoOrigine(String dataVoloRitornoOrigine) {
		this.dataVoloRitornoOrigine = dataVoloRitornoOrigine;
	}

	public String getDataVoloRitornoDestinazione() {
		return dataVoloRitornoDestinazione;
	}

	public void setDataVoloRitornoDestinazione(String dataVoloRitornoDestinazione) {
		this.dataVoloRitornoDestinazione = dataVoloRitornoDestinazione;
	}

	public int getNumeroAdulti() {
		return numeroAdulti;
	}

	public void setNumeroAdulti(int numeroAdulti) {
		this.numeroAdulti = numeroAdulti;
	}

	public int getNumeroBambini() {
		return numeroBambini;
	}

	public void setNumeroBambini(int numeroBambini) {
		this.numeroBambini = numeroBambini;
	}

	public String getNomeHotel() {
		return nomeHotel;
	}

	public void setNomeHotel(String nomeHotel) {
		this.nomeHotel = nomeHotel;
	}

	public String getDescrizioneHotel() {
		return descrizioneHotel;
	}

	public void setDescrizioneHotel(String descrizioneHotel) {
		this.descrizioneHotel = descrizioneHotel;
	}

	public String getRatingHotel() {
		return ratingHotel;
	}

	public void setRatingHotel(String ratingHotel) {
		this.ratingHotel = ratingHotel;
	}

	public int getStelleHotel() {
		return stelleHotel;
	}

	public void setStelleHotel(int stelleHotel) {
		this.stelleHotel = stelleHotel;
	}

	public String getIndirizzoHotel() {
		return indirizzoHotel;
	}

	public void setIndirizzoHotel(String indirizzoHotel) {
		this.indirizzoHotel = indirizzoHotel;
	}

	public String getImmagineHotel() {
		return immagineHotel;
	}

	public void setImmagineHotel(String immagineHotel) {
		this.immagineHotel = immagineHotel;
	}

	public String getLuogoOrigine() {
		return luogoOrigine;
	}

	public void setLuogoOrigine(String luogoOrigine) {
		this.luogoOrigine = luogoOrigine;
	}

	public String getLuogoDestinazione() {
		return luogoDestinazione;
	}

	public void setLuogoDestinazione(String luogoDestinazione) {
		this.luogoDestinazione = luogoDestinazione;
	}

	public String getNumeroHotel() {
		return numeroHotel;
	}

	public void setNumeroHotel(String numeroHotel) {
		this.numeroHotel = numeroHotel;
	}

	public int getIdHotel() {
		return idHotel;
	}

	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	
}


