package Service_Layer.ConnessioneServiziEsterniIMPL;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import Service_Layer.IVoli;

public class ProxyVoli implements IVoli{

	public String getVoli(String departure, String destination, String checkIn, String checkOut, String adults, String children) {
		
		String response = null;
		
		try {
			
		    HttpClient httpclient = HttpClients.createDefault();

	        URIBuilder builder = new URIBuilder("https://api.transavia.com/v1/flightoffers/");
            builder.setParameter("Origin", departure);
            builder.setParameter("Destination", destination);
            builder.setParameter("OriginDepartureDate", checkIn);
            builder.setParameter("DestinationDepartureDate", checkOut);
            builder.setParameter("Adults", adults); 
			builder.setParameter("Children", children);
			builder.setParameter("OrderBy", "Price");

            URI uri = builder.build();
            HttpGet request_voli = new HttpGet(uri);
            request_voli.setHeader("X-Deeplink-CultureCode", "it-IT");
            request_voli.setHeader("apikey", "f98dc607d4224ba99a77a1c526f75f9b");
            
            HttpResponse response_voli = httpclient.execute(request_voli);
            HttpEntity entity = response_voli.getEntity();
            
            if (entity == null) {
            	return null;
            }
            
            response = EntityUtils.toString(entity);
            
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	return response;
		
	}

}