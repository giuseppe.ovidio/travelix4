package Service_Layer.Service_Impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

import Data_Layer.DAOFactory;
import Data_Layer.Utenti.Utente;
import Data_Layer.Utenti.UtentiDAO;


public class LoginIMPL implements Service_Layer.ILogin{


	public String accedi(String username, String password) {

		String JSONUtente_str = new String();

		try {

			String hashedpass = DigestUtils.sha1Hex(password);
			JSONObject JSONUtente = new JSONObject();
			boolean isAuthenticated=false;

			DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.UTENTI);
			UtentiDAO riverDAO = (UtentiDAO) mysqlFactory.getMYSQLDAO();

			Utente utente = riverDAO.getUtente(username);

			String passdb = utente.getPassword();

			//controllo se l'hash della password appena calcolato
			//� uguale a quello contenuto nel db

			if(passdb.equals(hashedpass)) {
				isAuthenticated = true;
			}

			else
				System.out.println("autenticazione fallita!");

			if(isAuthenticated) {
				JSONUtente.put("esito", "true");
				JSONUtente.put("nome",utente.getNome());
				JSONUtente.put("ruolo",utente.getRuolo());
			}

			else {
				JSONUtente.put("esito", "false");
			}

			JSONUtente_str=JSONUtente.toString();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return JSONUtente_str;
	}
}