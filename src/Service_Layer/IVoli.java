package Service_Layer;

public interface IVoli {
	
	String getVoli(String departure, String destination, String checkIn, String checkOut, String adults, String children);

}