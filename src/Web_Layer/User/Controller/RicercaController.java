package Web_Layer.User.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service_Layer.IRicerca;
import Service_Layer.Service_Impl.RicercaIMPL;

/**
 * Servlet implementation class RicercaController
 */
//@WebServlet("/RicercaController")
public class RicercaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RicercaController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id_volo = new String ();

		id_volo = request.getParameter("ID_volo");

		if(id_volo==null) {
			doGet(request,response);
		}

		else {
			String id_hotel_str = new String (request.getParameter("ID_hotel"));
			int id_hotel = Integer.parseInt(id_hotel_str);

			doGet(request,response,id_hotel,id_volo);
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String departure = request.getParameter("departure"); //Luogo di partenza
		String destination = request.getParameter("destination"); //Luogo di arrivo
		String check_in_old = request.getParameter("check_in"); //Data partenza
		String check_out_old = request.getParameter("check_out"); //Data ritorno
		String adults = request.getParameter("adults"); //Numero adulti
		String children = request.getParameter("children"); //Numero bambini

		IRicerca ricercaIMPL = new RicercaIMPL();

		String JSONViaggi = ricercaIMPL.visualizzaCatalogoViaggi(departure, destination, check_in_old, check_out_old, adults, children);

		System.out.println("Catalogo: "+JSONViaggi);

		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		out.write(JSONViaggi);

		out.flush();
		out.close();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response, int id_hotel, String id_volo) throws ServletException, IOException {
		// TODO Auto-generated method stub

		IRicerca ricercaIMPL = new RicercaIMPL();

		String JSONViaggio = ricercaIMPL.visualizzaRiepilogoViaggio(id_hotel,id_volo);

		System.out.println("riepilogo: "+JSONViaggio);

		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		out.write(JSONViaggio);

		out.flush();
		out.close();

	}

}
